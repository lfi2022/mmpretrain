# mmpretrain
Git global setup
```sh
git config --global user.name 'flystarhe'
git config --global user.email 'flystarhe@qq.com'
```

Push an existing folder
```sh
git init --initial-branch=main
git remote add origin git@gitlab.com:lab2204/mmpretrain.git
git add .
git commit -m 'add README'
git push -u origin main
```

Create blank branch
```sh
git checkout --orphan 1.x
git rm -rf .
echo '# blank branch' > README.md
git add README.md
git commit -m 'new branch'
```

## mmpretrain v1.x
```sh
cd /data/workspace/mmpretrain
git clone git@gitlab.com:lab2204/mmpretrain.git .

# 2023/06/25
# - 4dd8a861456a88966f1672060c1f7b24f05ec363
# - Bump version to v1.0.0rc8 (#1583)
git fetch https://github.com/open-mmlab/mmpretrain.git main:1.x
git checkout 1.x
git push -u origin 1.x:1.x
```
